$("#toggle").on('click', function (e) {
    e.preventDefault();
    var side = $("#sidebar").toggleClass("toggled");
    $('#sidebar').toggle(1000);
    if (side.hasClass('toggled')) {
        $('.content-product').attr('style', 'flex:30%');
    } else {
        $('.content-product').removeAttr('style');
    }
});

$(".navbar-toggler").on('click', function (e) {
    e.preventDefault();
});

// $(document).ready(function () {
//     $(window).scroll(function () {
//         if ($(this).scrollTop() > 100) {
//             $('#topBtn').fadeIn();
//         } else {
//             $('#topBtn').fadeOut();
//         }
//     });

//     $('#topBtn').click(function () {
//         $('html, body').animate({
//                 scrollTop: 0
//             },
//             800
//         );
//     });
// });

// Scroll to top button appear
$(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
        $('#topBtn').fadeIn();
    } else {
        $('#topBtn').fadeOut();
    }
});

// Smooth scrolling using jQuery easing
$('#topBtn').click(function () {
    $('html, body').animate({
            scrollTop: 0
        },
        800
    );
});